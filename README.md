# IOC for LEBT vacuum residual gas analyzer

## Used modules

*   [vac_ctrl_halrcrga](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_halrcrga)


## Controlled devices

*   LEBT-010:Vac-VERA-10001
    *   LEBT-010:Vac-VGR-10000
