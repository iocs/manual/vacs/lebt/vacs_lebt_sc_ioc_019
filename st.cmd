#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_halrcrga
#
require vac_ctrl_halrcrga


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_halrcrga_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: LEBT-010:Vac-VGR-10000
# Module: vac_ctrl_halrcrga
#
iocshLoad("${vac_ctrl_halrcrga_DIR}/vac_ctrl_halrcrga_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VGR-10000, CONTROLLERNAME = LEBT-010:Vac-VERA-10001, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4013")

#- Temporary access security
asSetFilename("$(E3_CMD_TOP)/vacuum.acf")
asSetSubstitutions("HOSTNAME=$(HOSTNAME)")
